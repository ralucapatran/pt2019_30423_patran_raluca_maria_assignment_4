package BusinessLayer;

import java.util.ArrayList;
import java.util.Observable;

public class CompositeProduct extends Observable implements MenuItem {
	private ArrayList<MenuItem> menuItem = new ArrayList<MenuItem>();
	private ArrayList<BaseProduct> menuItems = new ArrayList<BaseProduct>();

	private String name;

	public CompositeProduct(String name, ArrayList<BaseProduct> menuItems) {

		this.menuItems = menuItems;
		this.name = name;

	}

	public float computePrice() {

		float number = 0;

		for (BaseProduct item : menuItems) {
			number += item.computePrice();
		}

		return number;

	}

	public void setCompositeName(String name1, String name2) {

		menuItems.get(0).setProductName(name1);
		menuItems.get(1).setProductName(name2);
	}

	public String getCompositeName() {
		return name;
	}

	public void addItem(MenuItem item) {
		menuItem.add(item);
	}

	public void removeItem(MenuItem item) {
		menuItem.remove(item);
	}

	public int getSize() {
		return menuItem.size();
	}

	public ArrayList<MenuItem> getList() {

		return menuItem;

	}

	public int contains(MenuItem item) {
		boolean blnFound = menuItem.contains(item);
		if (blnFound == true)
			return 1;
		else
			return -1;
	}

	public String getProductName() {
		return name;
	}

	public float getProductPrice() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setProductName(String name1) {
		this.name = name1;
		notifyObservers(new String(name));

	}

	public void setCompositePrice(float price1, float price2) {

		menuItems.get(0).setProductPrice(price1);
		menuItems.get(1).setProductPrice(price2);

		notifyObservers(new Float(price1));

	}

	@Override
	public void setProductPrice(float price) {
		// TODO Auto-generated method stub

	}

	public void update(Observable obs, Object obj) {
		if (obj instanceof String)
			System.out.println("The name has been changed! ");
		else if (obj instanceof Float)
			System.out.println("The price has been changed! ");
	}

}
