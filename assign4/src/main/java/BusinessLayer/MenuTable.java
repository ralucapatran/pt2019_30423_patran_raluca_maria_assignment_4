package BusinessLayer;

import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class MenuTable extends JFrame {
	private static final long serialVersionUID = 1L;

	public MenuTable(ArrayList<MenuItem> menuIt) {
		setTitle("Menu");
		setResizable(true);
		this.setBounds(400, 100, 800, 500);
		setLocationRelativeTo(null);
		init(menuIt);
	}

	public void init(ArrayList<MenuItem> men) {
		JPanel panel = new JPanel();

		String col[] = { "Product Name", "Product Price" };
		DefaultTableModel tableModel = new DefaultTableModel(col, 0);
		JTable table = new JTable(tableModel);
		table.setCellSelectionEnabled(false);
		table.setBounds(400, 100, 1000, 1000);

		for (MenuItem item : men) {
			Object[] objs = { item.getProductName(), item.computePrice() };
			tableModel.addRow(objs);
		}

		JScrollPane scroll = new JScrollPane(table);
		panel.add(table.getTableHeader());
		panel.add(scroll);

		setContentPane(panel);
		setVisible(true);
	}

}
