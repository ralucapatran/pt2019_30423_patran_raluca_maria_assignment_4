package BusinessLayer;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;

import java.util.Set;

public class Restaurant{

	private HashMap<Order, Set<MenuItem>> restaurant;
	
	public Restaurant (){
		restaurant = new HashMap<Order, Set<MenuItem>>();
		this.deserialize();
	}
	
	public HashMap<Order, Set<MenuItem>> getRestaurant() {
		return restaurant;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void deserialize() {
		try {
	         FileInputStream fileIn = new FileInputStream("Restaurant.ser");
	         ObjectInputStream in = new ObjectInputStream(fileIn);
	         restaurant = (HashMap)in.readObject();
	         in.close();
	         fileIn.close();
		}catch(IOException i) {
			i.printStackTrace();
		}catch(ClassNotFoundException c) {
			c.printStackTrace();
		}	
	}

	public void serialize() {
		try {
	         FileOutputStream fileOut = new FileOutputStream("Restaurant.ser");
	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
	         out.writeObject(restaurant);
	         out.close();
	         fileOut.close();
	         System.out.println("Datele au fost serializate");
		}catch(IOException i) {
	         i.printStackTrace();
	    }		
	}
	
	public boolean wellFormed() {
		for (Entry<Order, Set<MenuItem>> list : restaurant.entrySet()) 
		{
			if (list.getValue() == null || list.getValue().isEmpty()) 
			{
				return false;
			}
		}
		return true;
	}

	/*@pre order.getOrderId>=0;
	 *@inv isEmpty() implies restaurant.size==0; 
	 *@post restaurant.size() = restaurant.size() + 1;
	 */
	public void addOrder(Order order) {
		assert order!=null;
		int res_size=restaurant.size();
		assert wellFormed();
		restaurant.put(order,new HashSet<MenuItem>());
		assert wellFormed();
		assert res_size==restaurant.size()+1;
		this.serialize();
	}
	 
	public ArrayList<MenuItem> viewMenuItem() 
	{
		assert wellFormed();
		ArrayList<MenuItem> list_menu = new ArrayList<MenuItem>();
		for (Entry<Order, Set<MenuItem>> list : this.getRestaurant().entrySet())
		{
			for(MenuItem m:list.getValue())
			{
				list_menu.add(m);
			}
		}
		assert wellFormed();
		return list_menu;
	}
	
	 

}
