package BusinessLayer;

public class BaseProduct implements MenuItem {

	private String productName;
	private float productPrice;

	public BaseProduct(String productName, float productPrice) {

		this.productName = productName;
		this.productPrice = productPrice;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public float getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(float productPrice) {
		this.productPrice = productPrice;
	}

	public float computePrice() {
		return productPrice;

	}

	@Override
	public String getCompositeName() {
		return productName;
		// TODO Auto-generated method stub

	}

	@Override
	public void setCompositePrice(float price1, float price2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setCompositeName(String name1, String name2) {
		// TODO Auto-generated method stub

	}

}
