package BusinessLayer;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

public class Order implements Observer, Serializable {

	private static final long serialVersionUID = 1L;

	private int orderID;
	private String date;
	private int tableNo;

	public Order(int orderId, String date, int tableNo) {
		this.orderID = orderId;
		this.date = date;
		this.tableNo = tableNo;
	}

	public int getOrderID() {
		return orderID;
	}

	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getTableNo() {
		return tableNo;
	}

	public void setTableNo(int tableNo) {
		this.tableNo = tableNo;
	}

	@Override
	public void update(Observable o, Object obj) {
		if (obj instanceof Integer)
			System.out.println("The id has been changed! ");
		else if (obj instanceof String)
			System.out.println("The date has been changed! ");
		else if (obj instanceof Integer)
			System.out.println("The table numnber has been changed! ");
	}

}
