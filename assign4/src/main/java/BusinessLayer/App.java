package BusinessLayer;

import java.util.ArrayList;
import java.util.Objects;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

import PresentationLayer.AdminGUIAdd;
import PresentationLayer.AdminGUIDelete;
import PresentationLayer.AdminGUIEdit;
import PresentationLayer.GenerateBillGui;
import PresentationLayer.MainView;
import PresentationLayer.WaiterGUIAdd;
import DataLayer.FileWriter;

public class App {
	
	private AdminGUIAdd admGuiAdd;
	private AdminGUIDelete admGuiDelete;
	private AdminGUIEdit admGuiEdit;
	private WaiterGUIAdd waiterGuiAdd;
	private GenerateBillGui genBill;
	private MainView frame;
	private ArrayList<MenuItem> menuIt = new ArrayList<MenuItem>();
	private ArrayList<Order> orders = new ArrayList<Order>();

	public void start() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		frame = new MainView();
		admGuiAdd = new AdminGUIAdd();
		admGuiDelete = new AdminGUIDelete();
		admGuiEdit = new AdminGUIEdit();
		waiterGuiAdd = new WaiterGUIAdd();
		genBill = new GenerateBillGui();

		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		initializeButtonListeners();
	}

	public static void infoBox(String infoMessage, String titleBar) {
		JOptionPane.showMessageDialog(null, infoMessage, titleBar, JOptionPane.INFORMATION_MESSAGE);
	}

	public void initializeButtonListeners() {
		frame.addItemButtonActionListener(e -> {

			admGuiAdd.setVisible(true);
			admGuiAdd.setLocationRelativeTo(null);

		});

		frame.deleteItemButtonActionListener(e -> {

			admGuiDelete.setVisible(true);
			admGuiDelete.setLocationRelativeTo(null);

		});

		frame.editItemButtonActionListener(e -> {

			admGuiEdit.setVisible(true);
			admGuiEdit.setLocationRelativeTo(null);

		});

		admGuiAdd.addButtonActionListener(e -> {

			BaseProduct prod = new BaseProduct(admGuiAdd.getProductName(),
					Integer.parseInt(admGuiAdd.getProductPrice()));

			menuIt.add(prod);

		});

		admGuiAdd.addCompositeButtonActionListener(e -> {

			ArrayList<BaseProduct> products = new ArrayList<BaseProduct>();

			for (MenuItem item : menuIt) {
				if (Objects.equals(item.getProductName(), admGuiAdd.getProductName()) == true
						|| Objects.equals(item.getProductName(), admGuiAdd.getComposite()) == true) {
					products.add((BaseProduct) item);
				}
			}

			CompositeProduct compProd = new CompositeProduct(
					admGuiAdd.getCompositeName() + " : " + admGuiAdd.getProductName() + ", " + admGuiAdd.getComposite(),
					products);
			menuIt.add(compProd);

		});

		admGuiDelete.deleteButtonActionListener(e -> {

			for (MenuItem item : menuIt) {
				if (Objects.equals(item.getCompositeName(), admGuiDelete.getproductName()) == true) {
					menuIt.remove(item);
					System.out.println("Removed " + item.getCompositeName() + " " + item.computePrice());
				}

			}
		});

		admGuiEdit.editButtonActionListener(e -> {

			for (MenuItem item : menuIt) {
				if (Objects.equals(item.getProductName(), admGuiEdit.getOldName()) == true) {
					item.setProductName(admGuiEdit.getNewName());
					item.setCompositePrice(Integer.parseInt(admGuiEdit.getNewPrice()),
							Integer.parseInt(admGuiEdit.getNewPrice()));
					System.out.println("Changed " + item.getProductName() + " " + item.computePrice());

				}
			}

		});

		frame.createOrderButtonActionListener(e -> {

			waiterGuiAdd.setVisible(true);
			waiterGuiAdd.setLocationRelativeTo(null);
		});

		waiterGuiAdd.addOrderButtonActionListener(e -> {

			Order order = new Order(Integer.parseInt(waiterGuiAdd.getOrderiId()), waiterGuiAdd.getOrderDate(),
					Integer.parseInt(waiterGuiAdd.getTableNumber()));
			orders.add(order);

		});

		frame.viewOrdersButtonActionListener(e -> {

			OrderTable orTable = new OrderTable(orders);

			orTable.setVisible(true);
			orTable.setLocationRelativeTo(null);
		});

		frame.viewItemsButtonActionListener(e -> {

			MenuTable menTable = new MenuTable(menuIt);

			menTable.setVisible(true);
			menTable.setLocationRelativeTo(null);
		});

		frame.generateBillButtonActionListener(e -> {

			genBill.setVisible(true);
			genBill.setLocationRelativeTo(null);

		});

		genBill.generateBillButtonActionListener(e -> {

			FileWriter.createBill(Integer.parseInt(genBill.getOrderId()), orders, menuIt);

		});
	}
}
