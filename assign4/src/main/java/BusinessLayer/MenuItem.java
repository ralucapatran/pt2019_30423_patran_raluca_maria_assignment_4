package BusinessLayer;

public interface MenuItem {

	public float computePrice();

	public String getProductName();

	public float getProductPrice();

	public void setProductName(String name);

	public void setProductPrice(float price);

	public String getCompositeName();

	void setCompositePrice(float price1, float price2);

	public void setCompositeName(String name1, String name2);
}
