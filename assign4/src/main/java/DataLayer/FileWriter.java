package DataLayer;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;

import BusinessLayer.MenuItem;
import BusinessLayer.Order;

public class FileWriter {

	public static float ComputeTotalPrice(ArrayList<MenuItem> menuIt) {
		float totalPrice = 0;

		for (MenuItem m : menuIt) {
			totalPrice += m.computePrice();
		}

		return totalPrice;
	}

	public static void createBill(int orderId, ArrayList<Order> orders, ArrayList<MenuItem> menuIt) {
		try {

			PrintWriter writer = new PrintWriter("bill.txt");
			for (Order o : orders) {

				writer.print("OrderID: " + o.getOrderID());
				writer.print("\n");
				writer.print("Data: " + o.getDate());
				writer.print("\n");
				writer.print("Table: " + o.getTableNo());
				writer.print("\n");
				writer.print("Total: " + ComputeTotalPrice(menuIt) + " LEI");
				writer.print("\n");
				writer.print("\n");
				writer.print("Lista produse : ");
				writer.print("\n");
				writer.print("");
				for (MenuItem p : menuIt) {

					writer.print(p.getProductName() + "........price: " + p.computePrice() + " Lei");
					writer.print("\n");

				}
				writer.close();
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
