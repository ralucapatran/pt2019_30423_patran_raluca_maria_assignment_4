package PresentationLayer;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class AdminGUIEdit extends JFrame {

	private static final long serialVersionUID = 1L;
	private JTextField text1;
	private JTextField text2;
	private JTextField text3;
	private JTextField text4;
	
	private JLabel label1;
	private JLabel label2;
	private JLabel label3;
	private JLabel label4;
	
	private JButton btn;

	public AdminGUIEdit() {
		Font font1 = new Font("SansSerif", Font.BOLD, 20);

		this.setBounds(400, 400, 500, 300);
		getContentPane().setLayout(null);

		label1 = new JLabel("Old Product name");
		label1.setBounds(10, 10, 400, 100);
		label1.setFont(font1);
		getContentPane().add(label1);

		text1 = new JTextField();
		text1.setBounds(10, 110, 200, 40);
		text1.setFont(font1);
		getContentPane().add(text1);

		label2 = new JLabel("First base product price");
		label2.setBounds(10, 160, 400, 100);
		label2.setFont(font1);
		getContentPane().add(label2);

		text2 = new JTextField();
		text2.setBounds(10, 260, 200, 40);
		text2.setFont(font1);
		getContentPane().add(text2);

		label3 = new JLabel("New Product name");
		label3.setBounds(400, 10, 400, 100);
		label3.setFont(font1);
		getContentPane().add(label3);

		text3 = new JTextField();
		text3.setBounds(400, 110, 200, 40);
		text3.setFont(font1);
		getContentPane().add(text3);

		label4 = new JLabel("Second base product price");
		label4.setBounds(400, 160, 400, 100);
		label4.setFont(font1);
		getContentPane().add(label4);

		text4 = new JTextField();
		text4.setBounds(400, 260, 200, 40);
		text4.setFont(font1);
		getContentPane().add(text4);

		btn = new JButton("Edit!");
		btn.setBounds(10, 310, 120, 40);
		btn.setFont(font1);
		getContentPane().add(btn);

	}

	public void editButtonActionListener(final ActionListener actionListener) {
		btn.addActionListener(actionListener);
	}

	public String getOldName() {
		return text1.getText();
	}

	public String getNewName() {
		return text3.getText();
	}

	public String getNewPrice() {
		return text4.getText();
	}
}
