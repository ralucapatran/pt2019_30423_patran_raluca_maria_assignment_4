package PresentationLayer;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class AdminGUIAdd extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JTextField text1;
	private JTextField text2;
	private JTextField text3;
	private JTextField text4;

	private JLabel label1;
	private JLabel label2;
	private JLabel label3;
	private JLabel label4;

	private JButton btn;
	private JButton btn2;

	public AdminGUIAdd() {
		Font font1 = new Font("SansSerif", Font.BOLD, 20);

		this.setBounds(400, 400, 1000, 500);
		getContentPane().setLayout(null);

		label1 = new JLabel("Product name");
		label1.setBounds(10, 10, 250, 100);
		label1.setFont(font1);
		getContentPane().add(label1);

		text1 = new JTextField();
		text1.setBounds(10, 110, 200, 40);
		text1.setFont(font1);
		getContentPane().add(text1);

		label3 = new JLabel("Composite");
		label3.setBounds(410, 10, 250, 100);
		label3.setFont(font1);
		getContentPane().add(label3);

		text3 = new JTextField();
		text3.setBounds(410, 110, 200, 40);
		text3.setFont(font1);
		getContentPane().add(text3);

		label2 = new JLabel("Product price");
		label2.setBounds(10, 160, 250, 100);
		label2.setFont(font1);
		getContentPane().add(label2);

		text2 = new JTextField();
		text2.setBounds(10, 260, 200, 40);
		text2.setFont(font1);
		getContentPane().add(text2);

		label4 = new JLabel("Composite name");
		label4.setBounds(410, 160, 250, 100);
		label4.setFont(font1);
		getContentPane().add(label4);

		text4 = new JTextField();
		text4.setBounds(410, 260, 200, 40);
		text4.setFont(font1);
		getContentPane().add(text4);

		btn = new JButton("Add as base product!");
		btn.setBounds(10, 360, 250, 40);
		btn.setFont(font1);
		getContentPane().add(btn);

		btn2 = new JButton("Add as composite product!");
		btn2.setBounds(350, 360, 300, 40);
		btn2.setFont(font1);
		getContentPane().add(btn2);
	}

	public void addButtonActionListener(final ActionListener actionListener) {
		btn.addActionListener(actionListener);
	}

	public void addCompositeButtonActionListener(final ActionListener actionListener) {
		btn2.addActionListener(actionListener);
	}

	public String getProductName() {
		return text1.getText();
	}

	public String getProductPrice() {
		return text2.getText();
	}

	public String getComposite() {
		return text3.getText();
	}

	public String getCompositeName() {
		return text4.getText();
	}

}
