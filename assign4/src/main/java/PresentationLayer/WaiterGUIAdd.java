package PresentationLayer;

import java.awt.Font;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class WaiterGUIAdd extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField text1;
	private JTextField text2;
	private JTextField text3;

	private JLabel label1;
	private JLabel label2;
	private JLabel label3;

	private JButton btn2;

	public WaiterGUIAdd() {
		Font font1 = new Font("SansSerif", Font.BOLD, 20);

		this.setBounds(400, 400, 1000, 500);
		getContentPane().setLayout(null);

		label1 = new JLabel("Order id");
		label1.setBounds(10, 10, 250, 100);
		label1.setFont(font1);
		getContentPane().add(label1);

		text1 = new JTextField();
		text1.setBounds(10, 110, 200, 40);
		text1.setFont(font1);
		getContentPane().add(text1);

		label3 = new JLabel("Date");
		label3.setBounds(410, 10, 250, 100);
		label3.setFont(font1);
		getContentPane().add(label3);

		text3 = new JTextField();
		text3.setBounds(410, 110, 200, 40);
		text3.setFont(font1);
		getContentPane().add(text3);

		label2 = new JLabel("Table number");
		label2.setBounds(10, 160, 250, 100);
		label2.setFont(font1);
		getContentPane().add(label2);

		text2 = new JTextField();
		text2.setBounds(10, 260, 200, 40);
		text2.setFont(font1);
		getContentPane().add(text2);

		btn2 = new JButton("Add order!");
		btn2.setBounds(350, 360, 300, 40);
		btn2.setFont(font1);
		getContentPane().add(btn2);
	}

	public void addOrderButtonActionListener(final ActionListener actionListener) {
		btn2.addActionListener(actionListener);
	}

	public String getOrderiId() {
		return text1.getText();
	}

	public String getOrderDate() {
		return text3.getText();
	}

	public String getTableNumber() {
		return text2.getText();
	}

}
