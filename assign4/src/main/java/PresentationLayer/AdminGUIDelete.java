package PresentationLayer;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class AdminGUIDelete extends JFrame {

	private static final long serialVersionUID = 1L;
	private JTextField text1;
	private JLabel label1;
	private JButton btn;

	public AdminGUIDelete() {
		Font font1 = new Font("SansSerif", Font.BOLD, 20);

		this.setBounds(400, 400, 500, 800);
		getContentPane().setLayout(null);

		label1 = new JLabel("Product name");
		label1.setBounds(10, 10, 250, 100);
		label1.setFont(font1);
		getContentPane().add(label1);

		text1 = new JTextField();
		text1.setBounds(10, 110, 200, 40);
		text1.setFont(font1);
		getContentPane().add(text1);

		btn = new JButton("Delete!");
		btn.setBounds(10, 210, 200, 40);
		btn.setFont(font1);
		getContentPane().add(btn);
	}

	public void deleteButtonActionListener(final ActionListener actionListener) {
		btn.addActionListener(actionListener);
	}

	public String getproductName() {
		return text1.getText();
	}

}
