package PresentationLayer;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

public class MainView extends JFrame {

	private static final long serialVersionUID = 1L;

	private JPanel panel1;
	private JPanel panel2;
	
	private JButton btn1;
	private JButton btn2;
	private JButton btn3;
	private JButton btn4;
	private JButton btn5;
	private JButton btn6;
	private JButton btn8;

	public MainView() {

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(1000, 1000, 1500, 1500);

		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());

		getContentPane().add(mainPanel);

		itemTabPanel1();

		itemTabPanel2();

		JTabbedPane tabPane = new JTabbedPane();
		tabPane.addTab("Administrator", panel1);
		tabPane.addTab("Waiter", panel2);
		mainPanel.add(tabPane);

	}

	public void itemTabPanel1() {
		panel1 = new JPanel();
		panel1.setLayout(null);

		btn1 = new JButton("Create new menu item");
		btn1.setBounds(100, 110, 200, 100);
		panel1.add(btn1, BorderLayout.CENTER);

		btn2 = new JButton("Delete menu item");
		btn2.setBounds(100, 210, 200, 100);
		panel1.add(btn2);

		btn3 = new JButton("Edit menu item");
		btn3.setBounds(100, 310, 200, 100);
		panel1.add(btn3);

		btn8 = new JButton("View all menu items");
		btn8.setBounds(100, 410, 200, 100);
		panel1.add(btn8);
	}

	public void itemTabPanel2() {
		panel2 = new JPanel();
		panel2.setLayout(null);

		btn4 = new JButton("Create new order");
		btn4.setBounds(100, 110, 200, 100);
		panel2.add(btn4);

		btn5 = new JButton("Generate bill");
		btn5.setBounds(100, 210, 200, 100);
		panel2.add(btn5);

		btn6 = new JButton("View all orders");
		btn6.setBounds(100, 310, 200, 100);
		panel2.add(btn6);

	}

	public void addItemButtonActionListener(final ActionListener actionListener) {
		btn1.addActionListener(actionListener);
	}

	public void deleteItemButtonActionListener(final ActionListener actionListener) {
		btn2.addActionListener(actionListener);
	}

	public void editItemButtonActionListener(final ActionListener actionListener) {
		btn3.addActionListener(actionListener);
	}

	public void viewItemsButtonActionListener(final ActionListener actionListener) {
		btn8.addActionListener(actionListener);
	}

	public void createOrderButtonActionListener(final ActionListener actionListener) {
		btn4.addActionListener(actionListener);
	}

	public void generateBillButtonActionListener(final ActionListener actionListener) {
		btn5.addActionListener(actionListener);
	}

	public void viewOrdersButtonActionListener(final ActionListener actionListener) {
		btn6.addActionListener(actionListener);
	}

}
