package PresentationLayer;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class GenerateBillGui extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JTextField text1;
	private JLabel label1;

	private JButton btn2;

	public GenerateBillGui() {
		Font font1 = new Font("SansSerif", Font.BOLD, 20);

		this.setBounds(400, 400, 1000, 500);
		getContentPane().setLayout(null);

		label1 = new JLabel("Bill for order");
		label1.setBounds(10, 10, 250, 100);
		label1.setFont(font1);
		getContentPane().add(label1);

		text1 = new JTextField();
		text1.setBounds(10, 110, 200, 40);
		text1.setFont(font1);
		getContentPane().add(text1);

		btn2 = new JButton("Generate Bill!");
		btn2.setBounds(10, 210, 300, 40);
		btn2.setFont(font1);
		getContentPane().add(btn2);
	}

	public void generateBillButtonActionListener(final ActionListener actionListener) {
		btn2.addActionListener(actionListener);
	}

	public String getOrderId() {
		return text1.getText();
	}

}
